package com.hw10.task1;

import java.math.BigDecimal;

/**
 * <p>A class for interacting with a card.</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class ATM {

    private BigDecimal balance;
    private Card card;

    /**
     * <p>A constructor of Card.</p>
     */
    public ATM() {
        this(BigDecimal.ZERO);
    }

    /**
     * <p>A constructor of Card.</p>
     *
     * @param balance balance of the ATM.
     */
    public ATM(BigDecimal balance) {
        this.balance = balance;
        card = null;
    }


    /**
     * <p>Insert the card.</p>
     */
    public void insertCard(Card card) throws AtmException {
        if (valid()) {
            throw new AtmException("Card is already there");
        }
        this.card = card;
    }

    /**
     * <p>Take the card.</p>
     */
    public void takeCard() throws AtmException {
        if (!valid()) {
            throw new AtmException("No card");
        }
        this.card = null;
    }


    /**
     * <p>Fill the ATM.</p>
     */
    public void fillWithMoney(BigDecimal sum) throws AtmException {
        if (sum.signum() != 1) {
            throw new AtmException("Invalid sum");
        }
        balance = balance.add(sum);
    }

    /**
     * <p>Getting balance on the card.</p>
     *
     * @return balance on the card
     */
    public BigDecimal getBalanceOnCard() throws AtmException {
        if (!valid()) {
            throw new AtmException("Insert a card");
        }
        return card.getBalance();
    }

    /**
     * <p>Refill.</p>
     *
     * @param sum Sum of the refill.
     */
    public void add(BigDecimal sum) throws CardException, AtmException {
        if (!valid()) {
            throw new AtmException("Insert a card");
        }
        fillWithMoney(sum);
        card.add(sum);
    }

    /**
     * <p>Take money.</p>
     *
     * @param sum Sum of the withdrawal.
     */
    public void takeMoney(BigDecimal sum) throws CardException, AtmException {
        if (!valid()) {
            throw new AtmException("Insert a card");
        }
        if (sum.compareTo(balance) == 1 || sum.signum() == -1) {
            throw new AtmException("Not sufficient funds in the ATM. He has " + balance);
        }
        card.withDraw(sum);
        balance = balance.subtract(sum);
    }

    /**
     * <p>Check the card in the ATM.</p>
     *
     * @return true if ATM has a card.
     */
    public boolean valid() {
        return card != null;
    }

}