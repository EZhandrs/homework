package com.hw10.task1;

import java.math.BigDecimal;

public class AutoAtm extends Thread {
    String name;
    InterfaceOfAtm action;
    ATM atm;
    BigDecimal sum;
    Integer ms;
    Fight parent;

    public AutoAtm(String name, ATM atm, InterfaceOfAtm action, BigDecimal sum, Integer ms, Fight thread) {
        this.name = name;
        this.atm = atm;
        this.action = action;
        this.sum = sum;
        this.ms = ms;
        this.parent = thread;

        System.out.println("New thread: " + name + " " + action + " " + ms + " ms");
    }

    public void run() {
        try {
            while (true) {
                action.execute(atm, sum);
                System.out.println(name + ": " + action.toString() + " " + sum + " Balance: " + atm.getBalanceOnCard());
                if ((new BigDecimal("1000")).compareTo(atm.getBalanceOnCard()) != 1) {
                    System.out.println(name + " деньги есть");
                    parent.close();
                }
                Thread.sleep(ms);
            }
        } catch (CardException e) {
            System.out.println(name + " денег нет");
            parent.close();
        } catch (Exception e) {
            System.out.println("gg");
        }
    }
}