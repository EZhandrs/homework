package com.hw10.task1;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * <p>A class of a card</p>
 *
 * @author Oleg
 * @version 1.0
 */
public abstract class Card {

    protected String name;
    protected BigDecimal balance;

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name Name of the card.
     */
    public Card(String name) {
        this(name, BigDecimal.ZERO);
    }

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name    Name of the card.
     * @param balance Balance of the card.
     */
    public Card(String name, BigDecimal balance) {
        this.balance = balance;
        this.name = name;
    }

    /**
     * <p>With draw.</p>
     *
     * @param sum Sum of the withdrawal.
     */
    public abstract void withDraw(BigDecimal sum) throws CardException;

    /**
     * <p>Refill.</p>
     *
     * @param sum Sum of the refill.
     */
    public void add(BigDecimal sum) throws CardException {
        if (sum.signum() != 1) {
            throw new CardException("Invalid sum");
        }
        balance = balance.add(sum);
    }

    /**
     * <p>Getting the balance.</p>
     *
     * @return Balance of the card.
     */
    public BigDecimal getBalance() {
        return balance;
    }

}
