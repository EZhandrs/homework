package com.hw10.task1;

import java.math.BigDecimal;

/**
 * <p>A class of a Debit card</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class DebitCard extends Card {

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name    Name of the card.
     * @param balance Balance of the card.
     */
    public DebitCard(String name, BigDecimal balance) {
        super(name, balance);
    }

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name Name of the card.
     */
    public DebitCard(String name) {
        super(name);
    }

    @Override
    public void withDraw(BigDecimal sum) throws CardException {
        if (sum.compareTo(balance) == 1 || sum.signum() == -1) {
            throw new CardException("Not sufficient funds");
        }
        balance = balance.subtract(sum);
    }
}
