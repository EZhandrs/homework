package com.hw10.task1;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ExecutorService;

public class Fight extends Thread {
    String name;
    Card card;
    ExecutorService parent;

    public Fight(String name, Card card, ExecutorService parent) {
        this.name = name;
        this.card = card;
        this.parent = parent;
        System.out.println("New thread: " + name);
    }

    public void run() {
        ATM moneyProducerAtm = new ATM(new BigDecimal("1000"));
        moneyProducerAtm.insertCard(card);
        parent.submit(new AutoAtm(name, moneyProducerAtm, InterfaceOfAtm.ADD,
                new BigDecimal((new Random()).nextInt(6) + 5),
                ((new Random()).nextInt(4) + 2) * 1000, this));

        ATM moneyConsumerAtm = new ATM(new BigDecimal("1000"));
        moneyConsumerAtm.insertCard(card);
        parent.submit(new AutoAtm(name, moneyConsumerAtm, InterfaceOfAtm.WITHDRAW,
                new BigDecimal((new Random()).nextInt(6) + 5),
                ((new Random()).nextInt(4) + 2) * 1000, this));
    }

    public void close() {
        parent.shutdownNow();
    }
}
