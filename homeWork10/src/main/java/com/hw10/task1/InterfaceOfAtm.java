package com.hw10.task1;

import java.math.BigDecimal;
import java.util.Scanner;

public enum InterfaceOfAtm {
    WITHDRAW {
        @Override
        public void execute(ATM atm, BigDecimal sum) {
            atm.takeMoney(sum);
        }

        @Override
        public String toString() {
            return "with draw";
        }
    },
    ADD {
        @Override
        public void execute(ATM atm, BigDecimal sum) {
            atm.add(sum);
        }

        @Override
        public String toString() {
            return "refill the card";
        }
    };


    /**
     * <p>Execution of the selected operation.</p>
     */
    public abstract void execute(ATM atm, BigDecimal sum);

    /**
     * <p>Getting the name of the operation.</p>
     *
     * @return the name of the operation.
     */
    public abstract String toString();
}
