package com.hw10.task1;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.*;

public class Program {

    public static void main(String[] args) throws AtmException {
        int count = 3;
        String[] words = {"One", "Two", "Three", "Four", "Five"};
        Card myCard = new DebitCard("Visa", new BigDecimal(500));
        ExecutorService service = Executors.newCachedThreadPool();
        for (int i = 0; i < count; i++) {
            service.submit(new Fight(words[i], myCard, service));
        }
    }
}