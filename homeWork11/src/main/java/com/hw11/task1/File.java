package com.hw11.task1;

import java.io.Serializable;

public class File  implements Serializable {
    protected String name;
    protected String type;

    public File(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return name + "." + type;
    }
}
