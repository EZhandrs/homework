package com.hw11.task1;

import java.io.Serializable;
import java.util.ArrayList;

public class Folder implements Serializable {
    protected String name;
    protected ArrayList<Folder> folders;
    protected ArrayList<File> files;

    public Folder(String name) {
        this.name = name;
        this.folders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
    }

    @Override
    public String toString() {
        return name + "/";
    }
}
