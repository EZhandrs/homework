package com.hw11.task1;

import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Arrays;

public class HelperAdd {
    public static void add(Folder root, String path) {
        while (path.contains("//")) {
            path = path.replace("//", "/");
        }
        ArrayList<String> folders = new ArrayList<String>(Arrays.asList(path.split("/")));
        String[] file = new String[2];
        String last = Iterables.getLast(folders);
        if (last.contains(".")) {
            file[0] = last.substring(0, last.lastIndexOf('.'));
            file[1] = last.substring(last.lastIndexOf('.') + 1);
            folders.remove(folders.size() - 1);
        }
        add(find(root, folders), folders, file);
    }

    private static void add(Folder root, ArrayList<String> folders, String[] file) {
        for (String folder : folders) {
            root.folders.add(new Folder(folder));
            root = Iterables.getLast(root.folders);
        }
        if (file[0] != null) {
            root.files.add(new File(file[0], file[1]));
        }
    }

    private static Folder find(Folder root, ArrayList<String> folders) {
        for (Folder folder : root.folders) {
            if (folders.size() != 0 && folder.name.equals(folders.get(0))) {
                folders.remove(0);
                return find(folder, folders);
            }
        }
        return root;
    }
}
