package com.hw11.task1;

public class HelperPrint {
    static void Print(Folder folder) {
        Print(folder, 0);
    }

    private static void Print(Folder folder, int tab) {
        System.out.println( new String(new char[tab]).replace("\0", " ") + folder);
        for (File file : folder.files) {
            System.out.println(new String(new char[tab+2]).replace("\0", " ")+ file);
        }
        for (Folder folderChildren : folder.folders) {
            Print(folderChildren, tab + 2);
        }
    }
}
