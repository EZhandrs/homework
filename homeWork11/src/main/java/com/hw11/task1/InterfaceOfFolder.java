package com.hw11.task1;

import java.io.*;
import java.util.Scanner;

public class InterfaceOfFolder implements Serializable {
    public static void menu(Folder folder) throws IOException, ClassNotFoundException {
        Scanner in = new Scanner(System.in);
        String command;
        while (true) {
            command = in.nextLine();
            if (command.toUpperCase().equals("PRINT")) {
                HelperPrint.Print(folder);
            } else if (command.toUpperCase().equals("EXIT")) {
                break;
            } else if (command.toUpperCase().equals("SAVE")) {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                        new FileOutputStream("folder.out"));
                objectOutputStream.writeObject(folder);
                objectOutputStream.close();


            } else if (command.toUpperCase().equals("LOAD")) {
                ObjectInputStream objectInputStream = new ObjectInputStream(
                        new FileInputStream("folder.out"));
                folder = (Folder) objectInputStream.readObject();
                objectInputStream.close();
            } else {
                HelperAdd.add(folder, command);
            }
        }
    }
}
