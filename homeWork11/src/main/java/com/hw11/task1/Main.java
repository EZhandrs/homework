package com.hw11.task1;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Folder root = new Folder("root");
        InterfaceOfFolder.menu(root);
    }
}
