package com.hw3.task1;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * <p>A class for interacting with a card.</p>
 * @author Oleg
 *
 * @version 1.0
 */
public class ATM {

    /**
     * <p>Method for interacting with a card.</p>
     */
    public static void main(String[] args) throws SumException {
        Card myCard = new Card("Wood card");
        InterfaceOfAtm select;
        while (true) {
            try {
                select = InterfaceOfAtm.values()[EnumChoice.selectNumber(InterfaceOfAtm.class)];
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
            select.execute(myCard);
        }
    }
}