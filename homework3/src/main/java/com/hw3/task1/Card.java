package com.hw3.task1;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * <p>A class for using the card</p>
 * @author Oleg
 *
 * @version 1.0
 */
public class Card {

    private String name;
    private BigDecimal balance;

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name Name of the card.
     */
    public Card(String name) {
        this(name, BigDecimal.ZERO);
    }

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name Name of the card.
     * @param balance Balance of the card.
     */
    public Card(String name, BigDecimal balance) {
        this.balance = balance;
        this.name = name;
    }

    /**
     * <p>With draw.</p>
     *
     * @param sum Sum of the withdrawal.
     */
    public void withDraw(BigDecimal sum) throws SumException {
        if (sum.compareTo(balance) == 1 || sum.signum() == -1) {
            throw new SumException("Not sufficient funds");
        }
        balance = balance.subtract(sum);
    }

    /**
     * <p>Refill.</p>
     *
     * @param sum Sum of the refill.
     */
    public  void add(BigDecimal sum) throws SumException {
        if (sum.signum() != 1) {
            throw new SumException("Invalid sum");
        }
        balance = balance.add(sum);
    }

    /**
     * <p>Getting the balance.</p>
     *
     * @return Balance of the card.
     */
    public BigDecimal getBalance() {
        return  balance;
    }

    /**
     * <p>Getting the balance.</p>
     *
     * @param currency Currency for getting the balance
     * @return Balance of the card in the selected currency.
     */
    public BigDecimal getBalance(Currency currency) {
        return  balance.divide(currency.getExchangeRate(), new MathContext(8, RoundingMode.HALF_UP)
        );
    }
}
