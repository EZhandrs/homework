package com.hw3.task1;

import java.math.BigDecimal;

/**
 * <p>A enum of currencies.</p>
 * @author Oleg
 *
 * @version 1.0
 */
public enum Currency {
    USD("2.0732"),
    RUB("0.032"),
    EUR("2.27");

    private BigDecimal exchangeRate;

    Currency(String exchangeRate) {
        this.exchangeRate = new BigDecimal(exchangeRate);
    }

    /**
     * <p>Getting the exchange rate.</p>
     *
     * @return exchange rate.
     */
    public BigDecimal getExchangeRate() {
        return  exchangeRate;
    }
}
