package com.hw3.task1;

import java.util.EnumSet;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * <p>A class for choices the card</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class EnumChoice {

    /**
     * <p>Menu for select index.</p>
     *
     * @param enumType enum of select
     * @return index of the enum
     */
    public static <T extends Enum<T>> int selectNumber(Class<T> enumType) throws IndexException {
        int index;
        System.out.println("Select number");
        for (T enum_ : java.util.EnumSet.allOf(enumType)) {
            System.out.printf("%s - %s\n", enum_.ordinal() + 1, enum_.toString());
        }

        try {
            index = (new Scanner(System.in)).nextInt() - 1;
            if (index < 0 || index > EnumSet.allOf(enumType).toArray().length - 1) {
                throw new IndexException("Index out of range");
            }
        } catch (InputMismatchException e) {
            throw new IndexException("Select the correct number");
        }
        return index;
    }

}
