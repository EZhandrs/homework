package com.hw3.task1;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 * <p>A enum of card's operations.</p>
 * @author Oleg
 *
 * @version 1.0
 */
public enum InterfaceOfAtm {
    WithDraw {
        @Override
        public void execute(Card card) throws SumException {
            System.out.println("With draw");
            while (true) {
                System.out.println("Write the sum");
                try {
                    card.withDraw(new BigDecimal((new Scanner(System.in)).nextLine()));
                    break;
                } catch (SumException e) {
                    System.out.println(e.getMessage());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        @Override
        public String toString() {
            return "with draw";
        }
    },
    Add {
        @Override
        public void execute(Card card) throws SumException {
            System.out.println("Refill");
            while (true) {
                System.out.println("Write the sum");
                try {
                    card.add(new BigDecimal((new Scanner(System.in)).nextLine()));
                    break;
                } catch (SumException e) {
                    System.out.println(e.getMessage());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        @Override
        public String toString() {
            return "refill";
        }
    },
    GetBalance {
        @Override
        public void execute(Card card) {
            System.out.println("Balance");
            System.out.println(card.getBalance());
        }

        @Override
        public String toString() {
            return "balance";
        }
    },
    GetBalanceInAnotherCurrency {
        @Override
        public void execute(Card card) {
            System.out.println("Balance");
            Currency select;
            while (true) {
                try {
                    select = Currency.values()[EnumChoice.selectNumber(Currency.class)];
                    System.out.printf("%s %s\n",card.getBalance(select).toString(), select.toString());
                    break;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    continue;
                }
            }
        }

        @Override
        public String toString() {
            return "balance in another currency";
        }
    };

    /**
     * <p>Execution of the selected operation.</p>
     *
     * @return the name of the operat   ion.
     */
    public abstract void execute(Card card) throws SumException;

    /**
     * <p>Getting the name of the operation.</p>
     *
     * @return the name of the operation.
     */
    public abstract String toString();

}
