package com.hw3.task1;

/**
 * <p>A class of SumException.</p>
 * <p>If the sum is incorrect.</p>
 * @author Oleg
 *
 * @version 1.0
 */
public class SumException extends Exception {

    /**
     * <p>A constructor of SumException.</p>
     *
     * @param message Message of the r
     */
    public SumException(String message) {
        super(message);
    }
}
