package com.hw3.task2;

import java.util.Arrays;

/**
 * <p>A class for calculating a median</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class Median {

    /**
     * <p>Calculating the median.</p>
     *
     * @param array Array of int values
     * @return value of the median
     */
    public static float median(int[] array) {
        return (float) median(Arrays.stream(array).asDoubleStream().toArray());
    }

    /**
     * <p>Calculating the median.</p>
     *
     * @param array Array of double values
     * @return value of the median
     */
    public static double median(double[] array) {
        double[] temp = array.clone();
        Arrays.sort(temp);
        return temp.length % 2 == 1 ? temp[temp.length / 2] : (temp[temp.length / 2 - 1] + temp[temp.length / 2]) / 2;
    }
}
