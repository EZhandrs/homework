package com.hw4.task1;

/**
 * <p>A class of AtmException.</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class AtmException extends RuntimeException {

    /**
     * <p>A constructor of AtmException.</p>
     *
     * @param message Message of the exception
     */
    public AtmException(String message) {
        super(message);
    }
}
