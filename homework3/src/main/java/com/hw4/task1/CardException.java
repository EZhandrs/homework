package com.hw4.task1;

/**
 * <p>A class of SumException.</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class CardException extends RuntimeException {

    /**
     * <p>A constructor of SumException.</p>
     *
     * @param message Message of the exception
     */
    public CardException(String message) {
        super(message);
    }
}
