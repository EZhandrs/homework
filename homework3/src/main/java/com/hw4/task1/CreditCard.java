package com.hw4.task1;

import java.math.BigDecimal;

/**
 * <p>A class of a Credit card</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class CreditCard extends Card {
    private BigDecimal minBalance;

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name       Name of the card.
     * @param balance    Balance of the card.
     * @param minBalance Deposit of the card.
     */
    public CreditCard(String name, BigDecimal balance, BigDecimal minBalance) {
        super(name, balance);
        this.minBalance = minBalance;
    }

    /**
     * <p>A constructor of Card.</p>
     *
     * @param name Name of the card.
     */
    public CreditCard(String name) {
        super(name);
        minBalance = BigDecimal.ZERO;
    }

    @Override
    public void withDraw(BigDecimal sum) throws CardException {
        if (minBalance.compareTo(balance.subtract(sum)) == 1 || sum.signum() == -1) {
            throw new CardException("Not sufficient funds");
        }
        balance = balance.subtract(sum);
    }
}
