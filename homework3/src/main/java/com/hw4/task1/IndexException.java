package com.hw4.task1;

import java.util.EnumSet;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * <p>A class of IndexException.</p>
 * <p>If the index is incorrect.</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class IndexException extends RuntimeException {

    /**
     * <p>A constructor of IndexException.</p>
     *
     * @param message Message of the error.
     */
    public IndexException(String message) {
        super(message);
    }

}
