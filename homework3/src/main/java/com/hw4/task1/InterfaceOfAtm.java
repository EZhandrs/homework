package com.hw4.task1;

import java.math.BigDecimal;
import java.util.Scanner;

public enum InterfaceOfAtm {
    WITHDRAW {
        @Override
        public void execute(ATM atm) {
            System.out.println("With draw");
            while (true) {
                System.out.println("Write the sum");
                try {
                    atm.takeMoney(new BigDecimal((new Scanner(System.in)).nextLine()));
                    break;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    break;
                }
            }
        }

        @Override
        public String toString() {
            return "with draw";
        }
    },
    FILL_WITH_MONEY {
        @Override
        public void execute(ATM atm) {
            System.out.println("Fill with money");
            while (true) {
                System.out.println("Write the sum");
                try {
                    atm.fillWithMoney(new BigDecimal((new Scanner(System.in)).nextLine()));
                    break;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        @Override
        public String toString() {
            return "refill the ATM";
        }
    },
    ADD {
        @Override
        public void execute(ATM atm) {
            System.out.println("Refill");
            while (true) {
                System.out.println("Write the sum");
                try {
                    atm.add(new BigDecimal((new Scanner(System.in)).nextLine()));
                    break;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        @Override
        public String toString() {
            return "refill the card";
        }
    },
    GET_BALANCE {
        @Override
        public void execute(ATM atm) {
            System.out.println("Balance");
            try {
                System.out.println(atm.getBalanceOnCard());
            } catch (AtmException e) {
                System.out.println(e.getMessage());
            }
        }

        @Override
        public String toString() {
            return "balance";
        }
    },
    GET_BALANCE_IN_ANOTHER_CURRENCY {
        @Override
        public void execute(ATM atm) {
            System.out.println("Balance");
            Currency select;
            while (true) {
                try {
                    select = Currency.values()[EnumChoice.selectNumber(Currency.class)];
                    System.out.printf("%s %s\n", atm.getBalanceOnCard(select).toString(), select.toString());
                    break;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    continue;
                }
            }
        }

        @Override
        public String toString() {
            return "balance in another currency";
        }
    },
    TAKE_CARD {
        @Override
        public void execute(ATM atm) {
            System.out.println("Take card");
            try {
                atm.takeCard();
            } catch (AtmException e) {
                System.out.println(e.getMessage());
            }
        }

        @Override
        public String toString() {
            return "take card";
        }
    };


    /**
     * <p>Execution of the selected operation.</p>
     */
    public abstract void execute(ATM atm);

    /**
     * <p>Getting the name of the operation.</p>
     *
     * @return the name of the operation.
     */
    public abstract String toString();
}
