package com.hw4.task1;

import java.math.BigDecimal;

/**
 * <p>A class for testing</p>
 *
 * @author Oleg
 * @version 1.0
 */
public class Program {
    /**
     * <p>Method for interacting with a ATM.</p>
     */
    public static void main(String[] args) throws AtmException {
        Card myCard = new CreditCard("Wood card", new BigDecimal("100"), new BigDecimal("-100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        atm.insertCard(myCard);
        InterfaceOfAtm select;
        while (atm.valid()) {
            try {
                select = InterfaceOfAtm.values()[EnumChoice.selectNumber(InterfaceOfAtm.class)];
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
            select.execute(atm);
        }
    }
}
