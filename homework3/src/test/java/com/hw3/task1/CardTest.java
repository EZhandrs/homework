package com.hw3.task1;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
/**
 * Test suite for Median utility class.
 */
public class CardTest {

    @Test
    public void testConstructorOne() {
        Card card = new Card("Good");
        Assert.assertEquals(new BigDecimal("0"), card.getBalance());
    }

    @Test
    public void testConstructorTwo() {
        Card card = new Card("Good", new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("1"), card.getBalance());
    }

    @Test
    public void testWithDraw() throws SumException {
        Card card = new Card("Good", new BigDecimal("1"));
        card.withDraw(new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("0"), card.getBalance());
    }

    @Test
    public void testAdd() throws SumException {
        Card card = new Card("Good", new BigDecimal("1"));
        card.add(new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("2"), card.getBalance());
    }

    @Test
    public void testGetBalance() throws SumException {
        Card card = new Card("Good", new BigDecimal("1"));
        card.add(new BigDecimal("1"));
        card.withDraw(new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("1"), card.getBalance());
    }

    @Test
    public void testGetBalanceWithCurrency() throws SumException {
        Card card = new Card("Good", new BigDecimal("100"));
        Assert.assertEquals(new BigDecimal("3125"), card.getBalance(Currency.RUB));
    }

}
