package com.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test suite for Median utility class.
 */
public class AtmTest {

    @Test
    public void testWithDraw() throws CardException, AtmException {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        atm.insertCard(card);
        atm.takeMoney(new BigDecimal("100"));
        Assert.assertEquals(new BigDecimal("0"), atm.getBalanceOnCard());
    }

    @Test
    public void testAdd() throws CardException, AtmException {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        atm.insertCard(card);
        atm.add(new BigDecimal("100"));
        Assert.assertEquals(new BigDecimal("200"), atm.getBalanceOnCard());
    }

    @Test
    public void testGetBalance() throws AtmException {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        atm.insertCard(card);
        Assert.assertEquals(new BigDecimal("100"), atm.getBalanceOnCard());
    }

    @Test
    public void testGetBalanceWithCurrency() throws AtmException {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        atm.insertCard(card);
        Assert.assertEquals(new BigDecimal("3125"), atm.getBalanceOnCard(Currency.RUB));
    }

    @Test
    public void testValid() throws AtmException {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        ATM atm = new ATM(new BigDecimal("1000"));
        Assert.assertEquals(false, atm.valid());
        atm.insertCard(card);
        Assert.assertEquals(true, atm.valid());
    }
}
