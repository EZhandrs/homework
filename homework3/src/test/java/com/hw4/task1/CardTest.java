package com.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test suite for Median utility class.
 */
public class CardTest {

    @Test
    public void testConstructorOne() {
        Card creditCard = new CreditCard("Good");
        Card debitCard = new DebitCard("Good");
        Card x2card = new Card("SoGood") {
            @Override
            public void withDraw(BigDecimal sum) throws CardException {
                balance = balance.add(sum.add(sum));
            }
        };

        Assert.assertEquals(new BigDecimal("0"), creditCard.getBalance());
        Assert.assertEquals(new BigDecimal("0"), debitCard.getBalance());
        Assert.assertEquals(new BigDecimal("0"), x2card.getBalance());

    }

    @Test
    public void testConstructorTwo() {
        Card creditCard = new CreditCard("Good", new BigDecimal("100"), new BigDecimal("-100"));
        Card debitCard = new DebitCard("Good", new BigDecimal("100"));
        Card x2card = new Card("SoGood", new BigDecimal("100")) {
            @Override
            public void withDraw(BigDecimal sum) throws CardException {
                balance = balance.add(sum.add(sum));
            }
        };

        Assert.assertEquals(new BigDecimal("100"), creditCard.getBalance());
        Assert.assertEquals(new BigDecimal("100"), debitCard.getBalance());
        Assert.assertEquals(new BigDecimal("100"), x2card.getBalance());
    }

    @Test
    public void testWithDraw() throws CardException {
        Card card = new CreditCard("Good", new BigDecimal("1"), new BigDecimal("-100"));
        card.withDraw(new BigDecimal("2"));
        Assert.assertEquals(new BigDecimal("-1"), card.getBalance());
    }

    @Test
    public void testAdd() throws CardException {
        Card card = new DebitCard("Good", new BigDecimal("1"));
        card.add(new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("2"), card.getBalance());
    }

    @Test
    public void testGetBalance() throws CardException {
        Card tinkof = new Card("Ohh s", new BigDecimal("100")) {
            @Override
            public void withDraw(BigDecimal sum) throws CardException {
                balance = balance.subtract(sum.add(sum));
            }
        };
        tinkof.withDraw(new BigDecimal("10"));
        tinkof.withDraw(new BigDecimal("10"));
        Assert.assertEquals(new BigDecimal("60"), tinkof.getBalance());
    }

    @Test
    public void testGetBalanceWithCurrency() {
        Card card = new DebitCard("Good", new BigDecimal("100"));
        Assert.assertEquals(new BigDecimal("3125"), card.getBalance(Currency.RUB));
    }

}
